#
# ~/.bash_profile
#



# https://github.com/swaywm/sway/wiki/Running-programs-natively-under-Wayland
# Firefox
export MOZ_ENABLE_WAYLAND=1
# Bemenu
export BEMENU_BACKEND=wayland
# Qt5 (Okular)
export QT_QPA_PLATFORM=wayland-egl 
#export QT_WAYLAND_FORCE_DPI=physical
export QT_WAYLAND_DISABLE_WINDOWDECORATION=1
# LibreOffice
export SAL_USE_VCLPLUGIN=gtk3
# GTK decorations
#export GTK_CSD=0
# Instal arc-dark-solid and create config under .config/gtk3
#export GTK_THEME=Adwaita:dark
#export GTK_THEME=Arc-Dark-solid

# qt
# https://github.com/strawberrymusicplayer/strawberry/issues/391
QT_QPA_PLATFORMTHEME="qt5ct" 

# If running from tty1 start sway
if [ "$(tty)" = "/dev/tty1" ]; then
	exec sway
fi

[[ -f ~/.bashrc ]] && . ~/.bashrc
